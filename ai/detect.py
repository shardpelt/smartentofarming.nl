#Code for AI execution on the server

import time
import os
import shutil
import sys
import subprocess
import requests

state = 1
photo = sys.argv[1]

while (state == 1):
		#Execute detection on incoming image from client
        detection = subprocess.check_output('./darknet detect cfg/yolov3_custom.cfg yolov3_custom_final.weights ' + photo,shell=True)
        os.rename("predictions.jpg", "plant.jpg")
        print("RENAMED FILE SUCCESSFULLY")
        y=detection.splitlines()
        x=y[-1].decode(('utf-8'))
        values = x.split(':')
        percentage1 = values[1].replace(" ", "")
        percentage2 = percentage1.replace("%", "")
        percentage3 = int(percentage2)
		print(values[0])
		print(percentage3)
		
		#Sending image to database
		url = 'https://www.smartentofarming.nl/php/uploadPicture.php'
        filename = 'plant.jpg'
        path = sys.argv[1]
        image = {'file': open(filename, 'rb')}
        input = {'path': sys.argv[1], 'camera_id': '1', 'submit': 'done'}
        if values[0].find("Flower") != -1:
            input['flower'] = percentage3
        if values[0].find("Houseplant") != -1:
            input['houseplant'] = percentage3
        if values[0].find("Plant") != -1:
            input['plant'] = percentage3
        if values[0].find("Sprout") != -1:
            input['sprout'] = percentage3
        r = requests.post(url, files = image, data = input, verify = True)
        print(r.text)
        print(r)
        print("WAITING 12 HOURS FOR NEXT PICTURE")
        time.sleep(43200)