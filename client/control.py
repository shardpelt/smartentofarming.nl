#Code for taking picture of object and send it to the server

import picamera
import time
import os
import requests

state = 1

while (state == 1):
	#Camera code for taking pictures
	camera = picamera.PiCamera()
	camera.vflip = False
	camera.capture('plant.png')
	print("SAVED IMAGE")
	
	#Client code for sending images to server
	url = 'https://www.smartentofarming.nl/php/uploadPicture.php'
	print("ESTABLISHED LINK")
	filename = 'plant.png'
	print("PREPARING COMMUNICATION")
	image = {'file': open(filename, 'rb')}
	input = {'camera_id': 1, 'submit': 'done'}
	r = requests.post(url, files = image, data = input, verify = True)
	print(r.text)
	print(r)
	print("WAITING 12 HOURS FOR NEXT PICTURE")
	time.sleep(43200)