import Adafruit_DHT
import time
import paho.mqtt.client as mqtt

data = []
DHT_SENSOR = Adafruit_DHT.DHT11
DHT_PIN = 4

def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))

def on_log(mqttc, obj, level, string):
    print(string)

# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
# Uncomment to enable debug messages
#mqttc.on_log = on_log
mqttc.connect("192.168.0.100", 1883, 60)


while True:
    humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)

    if humidity is not None and temperature is not None:
        string = f'{humidity},{temperature}'
        arr = bytearray(string, 'utf-8')
        mqttc.publish("farm1/plant1", arr, qos=1)
    else:
        print("Er is iets fout gegaan")

    time.sleep(5)
