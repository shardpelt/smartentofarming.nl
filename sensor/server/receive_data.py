#Copyright (c) 2010-2013 Roger Light <roger@atchoo.org>
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Distribution License v1.0
# which accompanies this distribution.
#
# The Eclipse Distribution License is available at
#   http://www.eclipse.org/org/documents/edl-v10.php.
#
# Contributors:
#    Roger Light - initial implementation
# Copyright (c) 2010,2011 Roger Light <roger@atchoo.org>
# All rights reserved.

# This shows a simple example of an MQTT subscriber.

import paho.mqtt.client as mqtt
import MySQLdb

db = MySQLdb.connect(host = 'localhost', user = 'root', passwd = 'root', db = 'smartEntoFarming')
c = db.cursor()

query = """INSERT INTO Dht11_reading (dht11_id, timestamp, temperature, humidity) values(1, NOW(), %s, %s)"""

def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))

def on_message(mqttc, obj, msg):
    data = msg.payload.decode('utf-8')
    data = data.split(',')
    records = (float(data[1]), float(data[0]))
    try:
        c.execute(query, records)
        db.commit()
        print(f'humidity: {data[0]}, temperature: {data[1]}')
    except Exception as e: print(e)
def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)

# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Uncomment to enable debug messages
# mqttc.on_log = on_log
mqttc.connect("192.168.0.100", 1883, 60)
mqttc.subscribe("farm1/plant1", 0)
mqttc.loop_forever()

db.close()
