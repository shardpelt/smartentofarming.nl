<!-- template for site copied with permission from Jort Stuijt at jort.dev -->
<!DOCTYPE html>
<?php session_start();
?>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0, width=device-width">
		<title>Smart farming</title>
		<link rel="stylesheet" href="/css/stylesheet.css">
		<link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

	</head>
	<body>
		<div class="toolbar">
			<a class="active" href="https://www.smartentofarming.nl">Home</a>
			<a href="https://www.smartentofarming.nl/php/sprint.php">Sprint overzicht</a>
			<a href="https://www.smartentofarming.nl/php/productBacklog.php">Product backlog</a>
			<a href="https://www.smartentofarming.nl/html/login.html">Login</a>
			<a href="https://www.smartentofarming.nl/php/logout.php">Logout</a>
			<a href="https://www.smartentofarming.nl/php/register.php">Register</a>
                        <a href="https://www.smartentofarming.nl/php/dash.php">Dashboard</a>
			<a> logged in as: <?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} ?> </a>
		</div>
		<br>
		<div class="centered-flex-root-container">
			<div class="centered-flex-item">
				<h1 align="center">Project</h1>
				<h1>smart farming</h1> 
				<p>Group members</p>
				<ul>
					<li>
						Guido Ponson <a class="link" href="https://stud.hr.nl/0950071/">0950071</a>
					</li>
					<li>
						Krzysztof Cajler <a class="link" href="https://stud.hr.nl/0951562/">0951562</a>
					</li>
					<li>
						Arjan Ruigrok <a class="link" href="https://stud.hr.nl/0951894/">0951894</a>
					</li>
					<li>
						Nabil Chane <a class="link" href="https://stud.hr.nl/0940934/">0940934</a>
					</li>
				</ul>
				<a class="link-button" href="https://gitlab.com/shardpelt/smartentofarming.nl" target="_blank">GitLab</a>
				<a class="link-button link-button-inverse" href="https://trello.com/b/3asGoU3D/entofarming" target="_blank">Trello</a>
				<a class="link-button" href="https://drive.google.com/drive/folders/1jJcg1EbAok-USEZx0slWnt78H7PVim6P" target="_blank">Drive</a>
			</div>
		</div>
	</body>
</html>
