<!-- template for site copied with permission from Jort Stuijt at jort.dev -->
<!DOCTYPE html>
<?php session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function displayImages() {

	if(isset($_SESSION['admin'])){
		$dsn = 'mysql:dbname=smartEntoFarming;host=localhost;port=3306;charset=utf8';
		$connection = new \PDO($dsn, "root", "farmingishard");

		//throw exceptions, when SQL error is caused
		$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		// prevent emulation of prepared statements
		$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

		$statement = $connection->prepare("select file_path from Picture");
		//$statement->bindParam(':camera',$_POST["camera_id"],PDO::PARAM_INT);
		$statement->execute();
		while($result = $statement->fetch(PDO::FETCH_ASSOC)) {
			echo "<div class=\"imgCol\" ><img src=\"../pictures/".$result['file_path']."\" alt=\"please help me it's cold in here\" style=\"width:200px\"></div>";

		}
	}else {echo 'please log in as admin';}
}
?>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0, width=device-width">
		<title>Smart farming</title>
		<link rel="stylesheet" href="../css/stylesheet.css">
		<link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

	</head>
	<body>
		<div class="toolbar">
			<a href="https://www.smartentofarming.nl">Home</a>
			<a href="https://www.smartentofarming.nl/php/sprint.php">Sprint overzicht</a>
			<a href="https://www.smartentofarming.nl/php/productBacklog.php">Product backlog</a>
			<a href="https://www.smartentofarming.nl/html/login.html">Login</a>
			<a href="https://www.smartentofarming.nl/php/logout.php">Logout</a>
			<a href="https://www.smartentofarming.nl/php/register.php">Register</a>
			<a class="active" href="https://www.smartentofarming.nl/php/dash.php">Dashboard</a>
		        <a> logged in as: <?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} ?> </a>
</div>
<br>
				<div class="imgRow"><?php displayImages() ?></div>
	</body>
</html>
