<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if(isset($_SESSION['username'])){
	echo 'please logout first <br>';
} else {
	$dsn = 'mysql:dbname=smartEntoFarming;host=localhost;port=3306;charset=utf8';
	$connection = new \PDO($dsn, "root", "farmingishard");

	// throw exceptions, when SQL error is caused
	$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	// prevent emulation of prepared statements
	$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
	if(isset($_POST["submit"])){
		//check if user sent a username and password
		if(isset($_POST['password'])){
			if(isset($_POST['username'])){
				//if so prepare query with username and password
				$statement = $connection->prepare("SELECT * FROM Account where username = :usernamep");
				$statement->bindParam(':usernamep',$_POST['username'],PDO::PARAM_STR);
				$statement->execute();
				$results = $statement->fetchAll(PDO::FETCH_ASSOC);
				if(count($results)!=0){
					$password = $results[0]['password'];

					//check if result is empty, meaning there is no match
					if(!password_verify($_POST['password'],$password)){
						echo 'error invalid login';
						//TO DO log login attempt
					}else {
						#update user's lastLogin attribute in database
						$accountId = $results[0]["account_id"];
						$statement = $connection->prepare("update Account set last_login = NOW() where account_id = :accountId");
						$statement->bindParam(':accountId',$accountId,PDO::PARAM_INT);
						$statement->execute();
						echo 'login success';
						$_SESSION['username']=$_POST['username'];

						#check if user is admin, and if so set session variable for future checks.
						$statement = $connection->prepare("SELECT * FROM Admin where account_id = :id");
						$statement->bindParam(':id',$accountId,PDO::PARAM_INT);
						$statement->execute();
						$results = $statement->fetchAll(PDO::FETCH_ASSOC);
						if(isset($results[0]['account_id'])){
							$_SESSION['admin']='true';
						}

						header('Location: ../index.php');
						exit();
					}
				} else {echo 'incorrect login';}

			}else {echo 'error no username';}
		}else {echo 'error no password';}
	}
}
?>

