<!-- template for site copied with permission from Jort Stuijt at jort.dev -->
<!DOCTYPE html>
<?php session_start();
?>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0, width=device-width">
		<title>Smart farming</title>
		<link rel="stylesheet" href="../css/stylesheet.css">
		<link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

	</head>
	<body>
		<div class="toolbar">
			<a href="https://www.smartentofarming.nl">Home</a>
			<a href="https://www.smartentofarming.nl/php/sprint.php">Sprint overzicht</a>
			<a class="active" href="https://www.smartentofarming.nl/php/productBacklog.php">Product backlog</a>
			<a href="https://www.smartentofarming.nl/html/login.html">Login</a>
			<a href="https://www.smartentofarming.nl/php/logout.php">Logout</a>
			<a href="https://www.smartentofarming.nl/php/register.php">Register</a>
			<a href="https://www.smartentofarming.nl/php/dash.php">Dashboard</a>
			<a> logged in as: <?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} ?> </a>
		</div>
		<div class="docs-iframe">
			<iframe name="Product backlog document" id="iframeDocs" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQVgMJd6_Ej4kvFGp_OK7uJdV5ahu8q9RwUmTHJDBfRpo7AdXZ4OmbXBg4IBUF4rg/pubhtml?widget=true&amp;headers=false"></iframe>

		</div>
	</body>
</html>
