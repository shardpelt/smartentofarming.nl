<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dsn = 'mysql:dbname=smartEntoFarming;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "farmingishard");

// throw exceptions, when SQL error is caused
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
if(isset($_POST["submit"])){
	//check if user sent a username and password
	if(isset($_POST['password'])){
		if(isset($_POST['username'])){
			//if so prepare query with username and password
			$statement = $connection->prepare("SELECT * FROM Account where username = :usernamep");
			$statement->bindParam(':usernamep',$_POST['username'],PDO::PARAM_STR);
			$statement->execute();
			$results = $statement->fetchAll(PDO::FETCH_ASSOC);
			$username = $results[0]['username'];

			if(isset($username)){
				echo "username already taken, please try again";
			}elseif(empty($_POST['username'])){
				echo "please enter a valid username";
			}elseif(empty($_POST['password'])){
				echo "please enter a valid password";
			}else {
				$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
				
				#make account
				#$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
				$statement = $connection->prepare("insert into Account values(Null,:username,:password,NOW())");
				$statement->bindParam(':username',$_POST['username'],PDO::PARAM_STR);
				$statement->bindParam(':password',$password,PDO::PARAM_INT);
				$statement->execute();
				header('Location: /index.php');
				exit();
			}

		}else {echo 'error no username';}
	}else {echo 'error no password';}
}

?>

