<!-- template for site copied with permission from Jort Stuijt at jort.dev -->
<!DOCTYPE html>
<?php session_start();
?>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="initial-scale=1.0, width=device-width">
		<title>Smart farming</title>
		<link rel="stylesheet" href="../css/stylesheet.css">
		<link href="https://fonts.googleapis.com/css?family=Merriweather:300&display=swap" rel="stylesheet">

	</head>
	<body>
		<div class="toolbar">
			<a href="https://www.smartentofarming.nl">Home</a>
			<a class="active" href="https://www.smartentofarming.nl/html/sprint.html">Sprint overzicht</a>
			<a href="https://www.smartentofarming.nl/php/productBacklog.php">Product backlog</a>
			<a href="https://www.smartentofarming.nl/html/login.html">Login</a>
			<a href="https://www.smartentofarming.nl/php/logout.php">Logout</a>
			<a href="https://www.smartentofarming.nl/php/register.php">Register</a>
                        <a href="https://www.smartentofarming.nl/php/dash.php">Dashboard</a>
			<a> logged in as: <?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} ?> </a>
		</div>
		<div class="docs-iframe">
			<iframe name="sprint overzicht document" id="iframeDocs" src="https://docs.google.com/document/d/e/2PACX-1vRUrtKTlLEliv_765k2u5T-vbPy_o8n-mw_pXFKXUhrlOz2GybICTcYZ0Phl9SRumV1W_tsDZGa80qw/pub?embedded=true"></iframe>

		</div>
	</body>
</html>
