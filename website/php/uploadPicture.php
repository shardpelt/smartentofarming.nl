<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


if((isset($_POST['submit']))&&(isset($_POST['camera_id']))){
	if(array_key_exists('file',$_FILES)){
		if($_FILES['file']['error']===UPLOAD_ERR_OK){
			if(!$_FILES['file']['size']==0){

				$file = $_FILES['file'];
				$fileName = $_FILES['file']['name'];
				$fileTmpName = $_FILES['file']['tmp_name'];
				$fileSize = $_FILES['file']['size'];
				$fileError = $_FILES['file']['error'];
				$fileType = $_FILES['file']['type'];

				//get file extension, put that in a variable and
				//make it all lower case
				$fileExt = explode('.',$fileName);
				$fileActualExt = strtolower(end($fileExt));

				$allowed = array('jpg','jpeg','png');
				if(in_array($fileActualExt, $allowed)) {
					if($fileError === 0) {
						if($fileSize < 50000000) {
							//make new unique filename based on current time in
							//millis and then append the lowercase fileextension
							//to it
							$fileNameNew= uniqid('',true).".".$fileActualExt;
							$filePath = '../pictures';
							$fileDestination = $filePath.'/'.$fileNameNew;

							#save file to pictures folder, and add entry into the database.
							if(move_uploaded_file($fileTmpName,$fileDestination)){
								echo 'picture uploaded <br>';

								$dsn = 'mysql:dbname=smartEntoFarming;host=localhost;port=3306;charset=utf8';
								$connection = new \PDO($dsn, "root", "farmingishard");

								// throw exceptions, when SQL error is caused
								$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
								// prevent emulation of prepared statements
								$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
								$statement = $connection->prepare("insert into Picture values('0',NOW(),:path,:camera)");
								$statement->bindParam(':path',$fileDestination,PDO::PARAM_STR);
								$statement->bindParam(':camera',$_POST["camera_id"],PDO::PARAM_INT);
								$statement->execute();
								echo 'picture successfully added to database <br>';

								#if the uploaded picture was one processed by AI.
								if(isset($_POST['path'])){
									#save picture id of current pic for future use.
									$picId = $connection->lastInsertId();

									#path sent is relative path from ai folder, onverting to path from
									#local ../pictures folder
									$path = '..'.substr($_POST['path'],33);

									#get picture id from the original, non AI processed picture.
									$statement = $connection->prepare("select picture_id from Picture where file_path = :path");
									$statement->bindParam(':path',$path,PDO::PARAM_STR);
									$statement->execute();
									$res = $statement->fetchALL(PDO::FETCH_ASSOC);
									$oldId = $res[0]['picture_id'];

									#insert AI table attached to the processed picture.
									$statement = $connection->prepare("insert into AI_picture values(:pictureId,:origId)");
									$statement->bindParam(':pictureId',$picId,PDO::PARAM_INT);
									$statement->bindParam(':origId',$oldId,PDO::PARAM_INT);
									$statement->execute();
									echo 'ai picture successfully added to database <br>';

									echo 'printing post variable |';
									print_r($_POST);
									echo '|<br>';
									#insert results, if they are set by the ai program.
									if(isset($_POST['flower'])){
										$statement = $connection->prepare("insert into Flower values(:res,:pictureId)");
										$statement->bindParam(':res',$_POST['flower'],PDO::PARAM_INT);
										$statement->bindParam(':pictureId',$picId,PDO::PARAM_INT);
										$statement->execute();
										echo 'flower result added <br>';
									}
									if(isset($_POST['houseplant'])){
										$statement = $connection->prepare("insert into Houseplant values(:res,:pictureId)");
										$statement->bindParam(':res',$_POST['houseplant'],PDO::PARAM_INT);
										$statement->bindParam(':pictureId',$picId,PDO::PARAM_INT);
										$statement->execute();
										echo 'houseplant result added <br>';
									}
									if(isset($_POST['sprout'])){
										$statement = $connection->prepare("insert into Sprout values(:res,:pictureId)");
										$statement->bindParam(':res',$_POST['sprout'],PDO::PARAM_INT);
										$statement->bindParam(':pictureId',$picId,PDO::PARAM_INT);
										$statement->execute();
										echo 'sprout result added <br>';
									}
									if(isset($_POST['plant'])){
										$statement = $connection->prepare("insert into Plant values(:res,:pictureId)");
										$statement->bindParam(':res',$_POST['plant'],PDO::PARAM_INT);
										$statement->bindParam(':pictureId',$picId,PDO::PARAM_INT);
										$statement->execute();
										echo 'plant result added <br>';
									}
								} else {
									#TODO make calling ai program work.
									#run ai program with uploaded picture.
									#exec("cd ../../../AI/darknet && python detect.py ../../smartentofarming.nl/website".ltrim($fileDestination,2));
									#echo 'running ai on picture <br>';
									#
									#save file as plant.jpg to processedPics to the ai can find it,
									#right now the solution i sthat the ai loops and uses the same image
									#every hour
									$h = '../processedPics/plant.png';
									copy($fileDestination,$h);
									echo 'copied picture to ai folder <br>';

								}
							} else {echo 'failed to write file <br>';}
							#} else {echo 'failed to create folder <br>';}

						} else {echo 'filesize too large <br>';}
					} else {echo 'upload error <br>';}
				} else {echo 'wrong filetype <br>';}
			} else {echo 'no file found <br>';}

		} else {echo 'failure <br>';}
	} else {echo 'picture field empty <br>';}
} else {echo 'no picture sent <br>';}
?>

